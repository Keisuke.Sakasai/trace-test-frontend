package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"contrib.go.opencensus.io/exporter/jaeger"
	"go.opencensus.io/trace"

	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/plugin/ochttp/propagation/tracecontext"

	"github.com/gorilla/mux"
)

func mainHandler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Frontend APP #1!\n")

	requestPath := "http://go-app-svc-a.prod.svc.cluster.local"

	ctx, rootspan := trace.StartSpan(context.Background(), "/root_span")
	defer rootspan.End()
	rootspan.Annotate([]trace.Attribute{
		trace.StringAttribute("process", "frontend"),
	}, "Call frontend application.")

	ctx, childspan := trace.StartSpan(ctx, "/frontend_span")
	defer childspan.End()
	childspan.Annotate([]trace.Attribute{
		trace.StringAttribute("process", "frontend"),
	}, "Create Frontend ChildSpan.")

	time.Sleep(3 * time.Second)

	req, err := http.NewRequest("GET", requestPath, nil)
	if err != nil {
		log.Fatalf("%v", err)
	}

	childCtx, cancel := context.WithTimeout(ctx, 100000*time.Millisecond)
	defer cancel()
	req = req.WithContext(childCtx)

	format := &tracecontext.HTTPFormat{}
	format.SpanContextToRequest(childspan.SpanContext(), req)

	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("%v", err)
	}
	fmt.Printf("%v\n", res.StatusCode)
	fmt.Println(time.Now())
}

func main() {
	exporter, err := jaeger.NewExporter(jaeger.Options{
		AgentEndpoint: "jaeger-all-in-one-inmemory-agent:6831",
		Process: jaeger.Process{
			ServiceName: "Frontend APP",
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	defer exporter.Flush()

	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

	r := mux.NewRouter()
	r.HandleFunc("/", mainHandler)
	var handler http.Handler = r

	handler = &ochttp.Handler{
		Handler:     handler,
		Propagation: &tracecontext.HTTPFormat{}}

	log.Fatal(http.ListenAndServe(":8080", handler))
}
