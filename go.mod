module goland

go 1.17

require (
	contrib.go.opencensus.io/exporter/jaeger v0.2.1 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/uber/jaeger-client-go v2.25.0+incompatible // indirect
	go.opencensus.io v0.22.4 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
	google.golang.org/api v0.29.0 // indirect
)
